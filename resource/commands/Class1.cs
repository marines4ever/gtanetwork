﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTANetworkServer;
using GTANetworkShared;

namespace commands
{
    public class commands : Script
    {
        public commands()
        {
            API.onPlayerConnected += onPlayerConnected;
        }

        #region commands

        [Command("weapon")]
        public void weapon(Client player, WeaponHash weapon)
        {
            API.givePlayerWeapon(player, weapon, 9999, true, true);
        }

        [Command("car")]
        public void car(Client player, VehicleHash vehicle, int col, int col2)
        {
            Vector3 playerPos = API.getEntityPosition(player);
            Vector3 playerRot = API.getEntityRotation(player);
            API.createVehicle(vehicle, playerPos, playerRot, col, col2);
        }


        #endregion commands

        private void onPlayerConnected(Client player)
        {
            API.sendChatMessageToPlayer(player, "~y~ Commands Init");
        }
    }
}
